FROM node:13

MAINTAINER Artem Ilinykh devsinglesl@gmail.com

WORKDIR /app

COPY . .

RUN npm install

CMD npm run start
