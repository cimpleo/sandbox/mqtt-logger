import {connect as Connect, MqttClient as Client} from "mqtt";
import Dictionary from "./dictionary";
import HandlerInterface from "./handler.interface";
import ConfigInterface from "./config.interface";


export default class MqttClient {

    private readonly client: Client;
    private readonly handlers: Dictionary<HandlerInterface> = new Dictionary();

    public constructor(config: ConfigInterface){
        console.log('config', config);
        this.client = Connect(`${config.protocol}://${config.host}:${config.port}`, {
            rejectUnauthorized: config.rejectUnauthorized,
            username: config.username,
            password: config.password
        });
    }

    async subscribe(topic: string): Promise<void> {
        return new Promise((resolve, reject) => {
            this.client.subscribe(topic, err => {
                if(err){
                    return reject(err);
                }

                return resolve();
            })
        });
    }

    async unsubscribe(topic: string): Promise<void> {
        return new Promise(((resolve, reject) => {
            this.client.unsubscribe(topic, {}, (err) => {
               if(err){
                   return reject(err);
               }

               return resolve();
            });
        }))
    }

    async publish(topic: string, message: any): Promise<void> {
        return new Promise((resolve, reject) => {
            if(typeof message !=='string') {
                message = JSON.stringify(message);
            }
            this.client.publish(topic, message, (err) => {
               if(err){
                   return reject(err);
               }

               return resolve();
            });
        });

    }

    async multiPublish(topic: string, messages: any[]) : Promise<void> {
        await Promise.all(await messages.map(async message => await this.publish(topic, message)));

    }

    addHandler (handler: HandlerInterface) {
        this.handlers.add(handler.topic, handler);
    }

    async run(): Promise<void> {

        try {
            await this.handlers.forEachAsync(async (item) => {
                await this.subscribe(item.topic);
            });
        } catch (e) {
            console.log(e);
        }


        return new Promise(((resolve, reject) => {
            this.client.on('message', async (topic, message) => {

                if(this.handlers.keyExists(topic)){

                    try {
                        message = JSON.parse(message.toString());
                    }catch (e) {
                        /**
                         * TODO: fix this please!
                         */
                        // @ts-ignore
                        message = message.toString();
                    }
                    try {
                        console.log('working');
                        console.log('handlers', this.handlers);
                        await this.handlers.get(topic).handle(message);
                        return resolve();
                    } catch (e) {
                        return reject(e);
                    }
                }
            });
        }));
    }
}