export default interface HandlerInterface {
  topic: string;
  handle(message: any): Promise<void>;
}