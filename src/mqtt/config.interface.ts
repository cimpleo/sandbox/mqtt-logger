export default interface ConfigInterface {
  port: number;
  host: string;
  protocol: 'mqtt'|'mqtts';
  rejectUnauthorized: boolean;
  username: string;
  password: string;
}