export default class Dictionary<V> {
  private readonly dictionary: { [ key: string ]: V } = {};

  public add(key: string, value: V): void {
    this.dictionary[key] = value;
  }

  public findByKey(key: string): V|undefined {
    return this.dictionary[key];
  }

  public keyExists(key: string): boolean {
    return key in this.dictionary;
  }

  public remove(key: string): void {
    delete this.dictionary[key];
  }

  public get(key: string): V {
    if(!this.keyExists(key)) {
      throw new Error(`Key: ${key} is not exists in dictionary`);
    }

    return this.dictionary[key];
  }

  public getAll(): { [key: string]: V } {
    return this.dictionary;
  }

  public async forEachAsync(callable: (item: V, key: string, items: { [ key: string ]: V }) => Promise<void>): Promise<void> {
    for(let key in this.dictionary) {
      await callable(this.get(key), key, this.dictionary);
    }
  }
}