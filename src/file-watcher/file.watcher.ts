import * as fs from "fs";

export default class FileWatcher {

  private watcher: fs.FSWatcher;
  public onChange: (filename: string) => Promise<void> = async () => {};

  public constructor(
    private readonly path: string,
    private readonly options: { encoding?: string | null, persistent?: boolean, recursive?: boolean } | string | null
  ) {
    if(!fs.existsSync(path)){
      throw new Error(`File ${path} does not exists`);
    }
  }


  public async watch(): Promise<void> {
    return new Promise(((resolve, reject) => {
      this.watcher = fs.watch(this.path, this.options, async (eventType, filename) => {
        if(eventType === 'close') {
          return;
        }
        if(eventType === 'change') {
          await this.onChange(String(filename));
          return
        }
        if(eventType === 'error') {
          return reject('Watching error');
        }
      });
      return resolve();
    }));
  }

  public async close(): Promise<void> {
    this.watcher.close();
  }

}