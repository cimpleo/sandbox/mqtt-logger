import AppConfiguration from "./configuration/app.configuration";

(async () => {
  await (await AppConfiguration.configure()).run();
})();