import MqttClient from "../mqtt/mqtt.client";
import App from "../app/app";

export default new class Types {
  public readonly mqttConfigInterface = Symbol.for("mqttConfigInterface");
  public readonly mqttClient = Symbol.for("mqttClient");
  public readonly app = Symbol.for("app");
}