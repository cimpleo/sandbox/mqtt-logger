import "reflect-metadata";
import App from "../app/app";
import { Container } from "inversify";
import Types from "../configuration/types";
import MqttClient from "../mqtt/mqtt.client";
import ConfigInterface from "../mqtt/config.interface";

export default class AppConfiguration {
  public static async configure(): Promise<App> {

    const dotenv = require('dotenv');
    dotenv.config();

    const container = new Container();

    container.bind<ConfigInterface>(Types.mqttConfigInterface).toConstantValue({
      password: process.env.MQTT_PASSWORD,
      username: process.env.MQTT_USERNAME,
      rejectUnauthorized: false,
      protocol: process.env.MQTT_PROTOCOL,
      port: Number(process.env.MQTT_PORT),
      host: process.env.MQTT_DOMAIN,
    } as ConfigInterface);

    container.bind<MqttClient>(Types.mqttClient).toConstantValue(new MqttClient(
      container.get<ConfigInterface>(Types.mqttConfigInterface)
    ));



    container.bind<App>(Types.app).to(App);

    return container.get<App>(Types.app);
  }
}