import "reflect-metadata";
import {injectable, inject} from "inversify";
import Types from "../configuration/types";
import FileWatcher from "../file-watcher/file.watcher";
import * as path from "path";
import * as fs from "fs";
import ConfigInterface from "../logger/config/config.interface";
import TxtFormatter from "../logger/formatter/txt.formatter";
import HtmlFormatter from "../logger/formatter/html.formatter";
import MqttClient from "../mqtt/mqtt.client";
import HandlerInterface from "../mqtt/handler.interface";
import FormatterInterface from "../logger/formatter/formatter.interface";

@injectable()
export default class App {

  public constructor(
    @inject(Types.mqttClient) private readonly mqttClient: MqttClient
  ) {}

  public async run(): Promise<void> {




    const confPath = path.resolve(__dirname, "..", "..", "logger-config.json");
    const logFolderPath = path.resolve(__dirname, "..", "..", "logs");

    console.info('config changed');
    const configJson = JSON.parse(String( fs.readFileSync(confPath)));

    const configurations: ConfigInterface[] = configJson.map(conf => {
      let formatter: FormatterInterface;
      switch (conf.format) {
        case "txt":
          formatter = new TxtFormatter();
          break;
        case "html":
          formatter = new HtmlFormatter();
          break;
        default:
          formatter = new TxtFormatter();
      }

      return  {
        formatter,
        topic: conf.topic,
        filename: conf.filename + "." + formatter.extension,
        filepath: path.resolve(logFolderPath, conf.filename  + "." + formatter.extension)
      }
    });

    const handlers = configurations.map(config => {
      return new class implements HandlerInterface {
        topic: string = config.topic;

        async handle(message: any): Promise<void> {
          const string = `[${new Date()}]:${(await config.formatter.format(message))}\n`;
          fs.appendFileSync(config.filepath, string, {flag: "a"});
          console.log('write log', string);
        }
      };
    });

    handlers.forEach(handler => this.mqttClient.addHandler(handler));

    console.log('logger init');
    await this.mqttClient.run();


  }
}