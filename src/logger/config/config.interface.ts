import FormatterInterface from "../formatter/formatter.interface";

export default interface ConfigInterface {
  topic: string;
  filename: string;
  filepath: string;
  formatter: FormatterInterface;
}