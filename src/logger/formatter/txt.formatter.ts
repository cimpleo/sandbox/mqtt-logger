import FormatterInterface from "./formatter.interface";

export default class TxtFormatter implements FormatterInterface  {
  extension: string = 'txt';

  async format(log: string): Promise<string> {
    return log;
  }

}