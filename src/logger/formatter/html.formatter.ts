import FormatterInterface from "./formatter.interface";

export default class HtmlFormatter implements FormatterInterface {
  extension: string = 'html';

  async format(log: string): Promise<string> {
    return `<p>${log}<p>`;
  }

}