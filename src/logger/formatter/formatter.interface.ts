export default interface FormatterInterface {
  format(log: string): Promise<string>;
  extension: string;
}